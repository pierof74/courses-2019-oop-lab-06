package it.unibo.oop.lab06.generics1;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GraphImpl<N> implements Graph<N> {


  private final Map<N, Set<N>> nodes = new HashMap<N, Set<N>>();
  
  /**
   * {inheritDoc}
   */
  public void addNode(N node) {
    if (node != null) {
      if (!(this.nodes.containsKey(node))) {
        this.nodes.put(node, new HashSet<>());
      }
    }
  }

  /**
   * {inheritDoc}
   */
  public void addEdge(N source, N target) {
    if (this.containsBoth(source, target)){
      if (source!=target) {
        if (!(this.nodes.get(source).contains(target))) {
            this.nodes.get(source).add(target);
        }
      }
    }
  }

  /**
   * {inheritDoc}
   */
  public Set<N> nodeSet() {
    return this.nodes.keySet();
  }

  /**
   * {inheritDoc}
   */
  public Set<N> linkedNodes(N node) {
    if (this.nodes.containsKey(node)) {
      return this.nodes.get(node);
    } else {
      return Collections.emptySet();
    }
  }

  /**
   * {inheritDoc}
   */
  public List<N> getPath(N source, N target) {
    if (this.containsBoth(source, target)){
      LinkedList<N> retVal=new LinkedList<N>();
      this.getPathSafeFromCircularPath(source, target, retVal);
      return retVal;
    } else {
      return Collections.emptyList();
    }
  }

  private void getPathSafeFromCircularPath(final N source, final N target, LinkedList<N> history) {
    //System.out.println("exploring " + source);
    if (history.contains(source)) {
      //System.out.println("circular path " + history + " " + source);
      return;
    }
    history.add(source);
    if (source==target) {
      //System.out.println("final node matched " + history);
      return;
    }
    //System.out.println("possible next steps " + this.linkedNodes(source));
    for (N n: this.nodes.get(source)) {
      this.getPathSafeFromCircularPath(n, target, history);
      if (history.getLast()==target) {
        return;
      }
    }
    history.remove(source);
    return;
  }

  private boolean containsBoth(N n1, N n2) {
    return this.nodes.containsKey(n1) && (n1==n2 || this.nodes.containsKey(n2));
  }

  public String toString() {
    return this.nodes.toString();
  }
  
}
