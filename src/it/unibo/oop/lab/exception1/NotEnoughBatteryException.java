package it.unibo.oop.lab.exception1;

/**
 * Represents an exception occurring when a robot overcomes the boundaries of
 * its environment.
 * 
 */
public class NotEnoughBatteryException extends IllegalStateException {

    /**
     * The reason why this strange long field exists will be clear after the lesson
     * about I/O in Java...
     */
    private static final long serialVersionUID = 1L;
    /**
     * {@inheritDoc}
     */
    public String getMessage() {
        return "Battery is not enough";
    }
}
