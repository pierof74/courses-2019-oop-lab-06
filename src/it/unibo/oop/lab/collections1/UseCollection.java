package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
      long time = System.nanoTime();
      ArrayList<Integer> listInt = new ArrayList<Integer>();
      for(int i=1000;i<2000;i++) {
        listInt.add(i);
      }

      System.out.println((System.nanoTime()-time)+ " " + listInt);
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
      time = System.nanoTime();
      ArrayList<Integer> listInt2 = new ArrayList<Integer>(listInt);
      System.out.println((System.nanoTime()-time)+ " " + listInt2);
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
      time = System.nanoTime();
      Integer tmp = listInt.get(0);
      listInt.set(0, listInt.get(listInt.size()-1));
      listInt.set(listInt.size()-1, tmp);
      System.out.println((System.nanoTime()-time)+ " " + listInt);
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
      for(int i:listInt) {
        System.out.println(i);
      }
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
      time = System.nanoTime();
      ArrayList<Integer> arrListInt = new ArrayList<Integer>();
      for(int i=0; i<100;i++) {
        for(int j : listInt2) {
          arrListInt.add(0,j);
        }
      }
      System.out.println((System.nanoTime()-time)+ " Insert in 0 array list " + arrListInt.size());
      arrListInt.clear();
      time = System.nanoTime();
      for(int i=0; i<100;i++) {
        for(int j : listInt2) {
          arrListInt.add(j);
        }
      }
      System.out.println((System.nanoTime()-time)+ " insert at the end array list " + arrListInt.size());
      time = System.nanoTime();
      LinkedList<Integer> linkListInt = new LinkedList<Integer>();
      for(int i=0; i<100;i++) {
        for(int j : listInt2) {
          linkListInt.add(0,j);
        }
      }
      System.out.println((System.nanoTime()-time)+ " Insert in 0 linked list " + linkListInt.size());
      linkListInt.clear();
      time = System.nanoTime();
      for(int i=0; i<100;i++) {
        for(int j : listInt2) {
          linkListInt.add(j);
        }
      }
      System.out.println((System.nanoTime()-time)+ " insert at the end linked list " + linkListInt.size());
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        time = System.nanoTime();
        for(int i=0; i<1000;i++) {
          var j = arrListInt.get(50000);
        }
        System.out.println((System.nanoTime()-time)+ " read from the middle of array list");
        time = System.nanoTime();
        for(int i=0; i<1000;i++) {
          var j = linkListInt.get(50000);
        }
        System.out.println((System.nanoTime()-time)+ " read from the middle of linked list");
        
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        var continents = new java.util.HashMap<String,Long>(6);
        continents.put("Africa", 1110635000L);
        continents.put("Americas", 972005000L);
        continents.put("Antarctica", 0L);
        continents.put("Asia", 4298723000L);
        continents.put("Europe", 742452000L);
        continents.put("Oceania", 38304000L);
        System.out.println("Continents " + continents);
        /*
         * 8) Compute the population of the world
         */
        Long wordPop = 0L;
        for (var p: continents.values()) {
          wordPop += p;
          
        }
        System.out.println("Word population count " + wordPop);
        wordPop=0L;
        for (var c: continents.keySet()) {
          wordPop += continents.get(c);
          System.out.println("Word population count bis intermediate " + c + " " + wordPop);
          
        }
        System.out.println("Word population count bis " + wordPop);
        
    }
}
