package it.unibo.oop.lab.exception2;

public class NotEnoughFoundsException extends IllegalStateException {

  public NotEnoughFoundsException() {
    // TODO Auto-generated constructor stub
  }

  public NotEnoughFoundsException(String s) {
    super(s);
    // TODO Auto-generated constructor stub
  }

  public NotEnoughFoundsException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

  public NotEnoughFoundsException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

}
