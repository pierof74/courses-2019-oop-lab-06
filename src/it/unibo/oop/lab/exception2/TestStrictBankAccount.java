package it.unibo.oop.lab.exception2;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
        final double INITIAL_AMMOUNT = 1000d;
        final int MAX_TRANSACTIONS = 10;
        var ah1 = new AccountHolder("1", "1", 1);
        var ah2 = new AccountHolder("2", "2", 2);
        var sba1=new StrictBankAccount(ah1.getUserID(), INITIAL_AMMOUNT, MAX_TRANSACTIONS);
        var sba2=new StrictBankAccount(ah2.getUserID(), INITIAL_AMMOUNT, MAX_TRANSACTIONS);
        // test for WrongAccountHolderException
        try {
          sba1.withdrawFromATM(ah2.getUserID(), 1);
          fail("not failure for wrong userid");
        } catch (WrongAccountHolderException e) {
          assertNotNull(e.getMessage());
          assertFalse(e.getMessage().isEmpty());
        }
        // test for TransactionsOverQuotaException
        try {
          for (int i=0;i<MAX_TRANSACTIONS; i++) {
            sba1.withdrawFromATM(ah1.getUserID(), 1);
          }
        } catch (Exception e) {
          fail("failure for valid withdraw from atm " + e.getMessage());
        }
        try {
          sba1.withdrawFromATM(ah1.getUserID(), INITIAL_AMMOUNT/(MAX_TRANSACTIONS*2));
          fail("no failure for over quota transactions");
        } catch (TransactionsOverQuotaException e) {
          assertNotNull(e.getMessage());
          assertFalse(e.getMessage().isEmpty());
        }
        //test for NotEnoughFoundsException
        try {
          sba1.withdraw(ah1.getUserID(), INITIAL_AMMOUNT);
          fail("no failure for insufficent founds");
        } catch (NotEnoughFoundsException e) {
          assertNotNull(e.getMessage());
          assertFalse(e.getMessage().isEmpty());
        }
        
    }
}
