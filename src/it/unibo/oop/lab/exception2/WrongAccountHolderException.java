package it.unibo.oop.lab.exception2;

public class WrongAccountHolderException extends IllegalArgumentException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * 
   */

  public WrongAccountHolderException() {
    super();
  }

  public WrongAccountHolderException(String s) {
    super(s);
  }

  public WrongAccountHolderException(Throwable cause) {
    super(cause);
  }

  public WrongAccountHolderException(String message, Throwable cause) {
    super(message, cause);
  }

}
