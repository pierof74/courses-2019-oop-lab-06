package it.unibo.oop.lab.collections2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 * Instructions
 * 
 * This will be an implementation of
 * {@link it.unibo.oop.lab.collections2.SocialNetworkUser}:
 * 
 * 1) complete the definition of the methods by following the suggestions
 * included in the comments below.
 * 
 * @param <U>
 *            Specific user type
 */
public class SocialNetworkUserImpl<U extends User> extends UserImpl implements SocialNetworkUser<U> {

    /*
     * 
     * [FIELDS]
     * 
     * Define any necessary field
     * 
     * In order to save the people followed by a user organized in groups, adopt
     * a generic-type Map:
     * 
     * think of what type of keys and values would best suit the requirements
     */
    private final Map<String,Set<U>> followedUser= new HashMap<>();
    /*
     * [CONSTRUCTORS]
     * 
     * 1) Complete the definition of the constructor below, for building a user
     * participating in a social network, with 4 parameters, initializing:
     * 
     * - firstName - lastName - user name - age and every other necessary field
     * 
     * 2) Define a further constructor where age is defaulted to -1
     */

    /**
     * Builds a new {@link SocialNetworkUserImpl}.
     * 
     * @param name
     *            the user first name
     * @param surname
     *            the user last name
     * @param userAge
     *            user's age
     * @param user
     *            alias of the user, i.e. the way a user is identified on an
     *            application
     */
    public SocialNetworkUserImpl(final String name, final String surname, final String user, final int userAge) {
      super(name, surname, user, userAge);
    }
    
    public SocialNetworkUserImpl(final String name, final String surname, final String user) {
      this(name, surname, user, -1);
    }
    

    /*
     * [METHODS]
     * 
     * Implements the methods below
     */

    @Override
    public boolean addFollowedUser(final String circle, final U user) {
      if (!(this.followedUser.containsKey(circle))) {
        this.followedUser.put(circle, new HashSet<U>());
      }
      if (this.followedUser.get(circle).contains(user)) {
        return false;
      }
      return this.followedUser.get(circle).add(user);
    }
    
    @Override
    public Collection<U> getFollowedUsersInGroup(final String groupName) {
      if (this.followedUser.containsKey(groupName)) {
      return new HashSet<U>(this.followedUser.get(groupName));
      } else {
        return new HashSet<U>();
      }
    }

    @Override
    public List<U> getFollowedUsers() {
      int folCount = 0;
      for (Set<U> g : this.followedUser.values()) {
        folCount += g.size();
      }
      List<U> retVal = new ArrayList<U>(folCount);
      for (Set<U> g : this.followedUser.values()) {
        for (U u:g) {
          retVal.add(u);
        }
      }
      return retVal;
    }

}
